/* eslint-disable linebreak-style */
/* eslint-disable no-dupe-keys */
/* eslint-disable import/no-unresolved */
/* eslint-disable linebreak-style */
/* eslint-disable import/no-extraneous-dependencies */
const { defineConfig } = require('cypress');
const preprocessor = require('@badeball/cypress-cucumber-preprocessor');
const browserify = require('@badeball/cypress-cucumber-preprocessor/browserify');
const { addMatchImageSnapshotPlugin } = require('cypress-image-snapshot/plugin');
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const { configureAllureAdapterPlugins } = require('@mmisty/cypress-allure-adapter/plugins');

async function setupNodeEvents(on, config) {
  await preprocessor.addCucumberPreprocessorPlugin(on, config);

  on('file:preprocessor', browserify.default(config));
  addMatchImageSnapshotPlugin(on, config);
  allureWriter(on, config);
  configureAllureAdapterPlugins(on, config);

  return config;
}

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    setupNodeEvents,
    baseUrl: 'https://www.qrcode-monkey.com/',
    specPattern: 'cypress/e2e/specs/*.feature',
    viewportWidth: 1280,
    viewportHeight: 800,
    experimentalSessionSupport: true,
    pageLoadTimeout: 20000,
    defaultCommandTimeout: 20000,
    retries: 2,
    video: false,
    chromeWebSecurity: false,
    allure: true,
  },
});
