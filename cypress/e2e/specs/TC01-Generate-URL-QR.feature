Feature: Create QR code for the given URL

As a product owner, I would like to add the capability to generate the QR code via URL so that the user can store and transmit data easily for the given web address.
    
    @1
    Scenario Outline: Look & feel of the 'URL' tab
        When user visits '/' on '<viewport>' device
        Then user should land on to the home page 
        And user sees the following information to be true:
        * the look & feel of the home page should be as per '<viewport>' design
        * 'URL' to be the default active tab having tooltip text as 'Create QR Code for a Website'
        * URL placeholder text should be 'https://www.qrcode-monkey.com'
        * default QR code size to be '1000 x 1000'
        * 'Create QR Code' button to be in enabled state while the 'Download PNG', '.SVG', '.PDF*' and '.EPS*' buttons to be in disabled state by default
        
        Examples:

            | viewport |
            | desktop  |
            | tablet   |
            | mobile   |
    
    @2
    Scenario Outline: Ability to create new QR code for the given URLs
        Given user visits '/' on '<viewport>' device
        When user create a new QR code for the given '<URL>'
        Then user should see the new QR code to be generated appropriately pointing to the same '<URL>'
    
        Examples:

            | viewport | URL                          |
            | desktop  | http://www.google.com        |
            | mobile   | http://www.google.com        |
            | desktop  | https://www.google.com       |
            | mobile   | https://www.google.com       |
            | desktop  | ftp://ftp.example.com        |
            | mobile   | ftp://ftp.example.com        |
            | desktop  | smtp://mail.example.com      |
            | mobile   | smtp://mail.example.com      |
            | desktop  | telnet://example.com         |
            | mobile   | telnet://example.com         |
            | desktop  | ssh://example.com            |
            | mobile   | ssh://example.com            |
            | desktop  | ftps://ftp.example.com       |
            | mobile   | ftps://ftp.example.com       |
            | desktop  | sftp://example.com           |
            | mobile   | sftp://example.com           |
            | desktop  | ldap://directory.example.com |
            | mobile   | ldap://directory.example.com |

    @3
    Scenario: Ability to create new QR code for the given URL using enter keystroke
        Given user visits '/' on 'desktop' device
        When user create a new QR code for the given '<URL>' using enter key thorugh keyboard
        Then user should see the new QR code to be generated appropriately pointing to the same '<URL>'
    
        Examples:

            | URL                    |
            | http://www.google.com  |
            | http://www.yahoo.com   |
    
    @4
    Scenario Outline: Inability to create new QR code for an invalid URL pattern
        Given user visits '/' on '<viewport>' device
        When user tries to create a new QR code for an invalid '<URL>' pattern
        Then user should see two error messages as 'Enter a valid URL' & 'There are errors you have to fix before generating.' indicating to correct the user's input
        
        Examples:

            | viewport | URL               |
            | desktop  | http://           |
            | mobile   | http://           |
            | desktop  | ://www.google.com |
            | mobile   | ://www.google.com |
    
    @5
    Scenario: Inability to create new QR code if the input is blank
        Given user visits '/' on '<viewport>' device
        When user tries to create a new QR code without entering the URL
        Then user should see two error messages as 'Enter a valid URL' & 'There are errors you have to fix before generating.' indicating to correct the user's input

        Examples:

            | viewport | 
            | desktop  | 
            | mobile   |
    
    @6
    Scenario Outline: Create QR button & download button behavior
        Given user visits '/' on '<viewport>' device
        And user finds the 'Download PNG', '.SVG', '.PDF*' and '.EPS*' buttons to be disabled
        When user clicks on 'Create QR Code' button to generate the new QR code
        Then user finds the 'Download PNG', '.SVG', '.PDF*' and '.EPS*' buttons to be enabled while the 'Create QR Code' button to be disabled
        But on updating the URL with an input character 'a'
        Then the 'Create QR Code' button to be enabled back again along with other download buttons
        
        Examples:

            | viewport |
            | desktop  |
            | mobile   |
    
    @7
    Scenario Outline: Download QR code in various available formats such as PNG, SVG, PDF and EPS
        Given user visits '/' on '<viewport>' device
        When user create a new QR code for the given URL 'https://www.google.com'
        And download it in '<file_type>' format
        Then user should see a success message as 'Your QR Code is being Generated. Please do not refresh or close the window.'
        And the QR code should get downloaded successfully in '<file_type>' format pointing to the same URL 'https://www.google.com' 
        
        Examples:

            | viewport | file_type |
            | desktop  | png       |
            | mobile   | png       |
            | desktop  | svg       |
            | mobile   | svg       |