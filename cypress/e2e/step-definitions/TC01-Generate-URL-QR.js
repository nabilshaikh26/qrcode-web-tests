/* eslint-disable linebreak-style */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable cypress/unsafe-to-chain-command */
/* eslint-disable linebreak-style */
/// <reference types="cypress" />
import { When, Then } from '@badeball/cypress-cucumber-preprocessor';
import { Decoder } from '@nuintun/qrcode';
import Home from '../ui-identifiers/Home';

const qrcode = new Decoder();
const home = new Home();

// Scenario-1

Then('user should land on to the home page', () => {
  cy
    .url()
    .should('equal', Cypress.config('baseUrl'));
});

When('user sees the following information to be true:', () => {});

Then('the look & feel of the home page should be as per {string} design', (viewport) => {
  home
    .getQRCodeForm()
    .matchImageSnapshot(
      `URL-Tab-${viewport}`,
      {
        failureThreshold: 1,
        failureThresholdType: 'percent',
      },
    );
});

Then("'URL' to be the default active tab having tooltip text as {string}", (tooltipText) => {
  home
    .getActiveTabTitle()
    .should((getTitle) => {
      expect(getTitle).equal(tooltipText);
    });
});

Then('URL placeholder text should be {string}', (placeholderText) => {
  home
    .getURLTextbox()
    .should((placeholder) => {
      expect(placeholder).to.have.value(placeholderText);
    });
});

Then('default QR code size to be {string}', (qrCodeSize) => {
  cy
    .contains(qrCodeSize);
});

Then("'Create QR Code' button to be in enabled state while the 'Download PNG', '.SVG', '.PDF*' and '.EPS*' buttons to be in disabled state by default", () => {
  home
    .createQRCodeButton()
    .should('be.enabled');
  home
    .downloadQRCodeInPNGButton()
    .should('be.disabled');
  home
    .downloadQRCodeInSVGButton()
    .should('be.disabled');
  home
    .downloadQRCodeInPDFButton()
    .should('be.disabled');
  home
    .downloadQRCodeInEPSButton()
    .should('be.disabled');
});

// Scenario-2

When('user create a new QR code for the given {string}', (url) => {
  home
    .getURLTextbox()
    .type('{ctrl+A}')
    .clear()
    .type(url);
  home
    .createQRCodeButton()
    .click();
});

Then('user should see the new QR code to be generated appropriately pointing to the same {string}', (url) => {
  home
    .getQRCodePreview()
    .invoke('attr', 'src')
    .should('not.contain', 'default');
  home
    .getQRCodePreview()
    .invoke('attr', 'src')
    .then((imageURL) => {
      qrcode.scan(`https:${imageURL}`)
        .then((result) => {
          expect(result.data).to.eq(url);
        });
    });
});

// Scenario-3

When('user create a new QR code for the given {string} using enter key thorugh keyboard', (url) => {
  home
    .getURLTextbox()
    .type('{ctrl+A}')
    .clear()
    .type(url)
    .type('{enter}');
});

// Scenario-4

When('user tries to create a new QR code for an invalid {string} pattern', (url) => {
  home
    .getURLTextbox()
    .type('{ctrl+A}')
    .clear()
    .type(url)
    .type('{enter}');
});

Then("user should see two error messages as {string} & {string} indicating to correct the user's input", (blankURLError, genericError) => {
  home
    .getIncorrectURLError()
    .then((error) => {
      expect(error.text().trim()).to.equal(blankURLError);
    });
  home
    .getAlertMessage()
    .then((error) => {
      expect(error.text().trim()).to.equal(genericError);
    });
});

// Scenario-5

When('user tries to create a new QR code without entering the URL', () => {
  home
    .getURLTextbox()
    .type('{ctrl+A}')
    .clear()
    .type('{enter}');
});

// Scenario-6

When("user finds the 'Download PNG', '.SVG', '.PDF*' and '.EPS*' buttons to be disabled", () => {
  home
    .downloadQRCodeInPNGButton()
    .should('be.disabled');
  home
    .downloadQRCodeInSVGButton()
    .should('be.disabled');
  home
    .downloadQRCodeInPDFButton()
    .should('be.disabled');
  home
    .downloadQRCodeInEPSButton()
    .should('be.disabled');
});

When("user clicks on 'Create QR Code' button to generate the new QR code", () => {
  home
    .createQRCodeButton()
    .click();
});

Then("user finds the 'Download PNG', '.SVG', '.PDF*' and '.EPS*' buttons to be enabled while the 'Create QR Code' button to be disabled", () => {
  home
    .downloadQRCodeInPNGButton()
    .should('be.enabled');
  home
    .downloadQRCodeInSVGButton()
    .should('be.enabled');
  home
    .downloadQRCodeInPDFButton()
    .should('be.enabled');
  home
    .downloadQRCodeInEPSButton()
    .should('be.enabled');
  home
    .createQRCodeButton()
    .should('be.disabled');
});

Then('on updating the URL with an input character {string}', (character) => {
  home
    .getURLTextbox()
    .type(character);
});

Then("the 'Create QR Code' button to be enabled back again along with other download buttons", () => {
  home
    .createQRCodeButton()
    .should('be.enabled');
  home
    .downloadQRCodeInPNGButton()
    .should('be.enabled');
  home
    .downloadQRCodeInSVGButton()
    .should('be.enabled');
  home
    .downloadQRCodeInPDFButton()
    .should('be.enabled');
  home
    .downloadQRCodeInEPSButton()
    .should('be.enabled');
});

// Scenario-7

When('user create a new QR code for the given URL {string}', (url) => {
  home
    .getURLTextbox()
    .type('{ctrl+A}')
    .clear()
    .type(url)
    .type('{enter}');
});

Then('download it in {string} format', (fileFormat) => {
  switch (fileFormat) {
    case 'png':
      home
        .downloadQRCodeInPNGButton()
        .click();
      break;
    case 'svg':
      home
        .downloadQRCodeInSVGButton()
        .click();
      break;
    case 'pdf':
      home
        .downloadQRCodeInPDFButton()
        .click();
      break;
    case 'eps':
      home
        .downloadQRCodeInEPSButton()
        .click();
      break;
    default:
      throw new Error('Invalid Format Selected!');
  }
});

Then('user should see a success message as {string}', (successMessage) => {
  home
    .downloadQRCodeStatusText()
    .should('contain.text', successMessage);
});

Then('the QR code should get downloaded successfully in {string} format pointing to the same URL {string}', (format, url) => {
  cy
    .readDownloadedFile(format)
    .then((imageSrc) => {
      qrcode.scan(imageSrc)
        .then((result) => {
          expect(result.data).to.eq(url);
        });
    });
});
