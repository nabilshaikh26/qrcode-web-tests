/* eslint-disable linebreak-style */
/* eslint-disable class-methods-use-this */
class Home {
  getQRCodeForm() {
    return cy.get('.qrcode-generator');
  }

  getActiveTabTitle() {
    return cy.get('.type-bar-inner').find('.active').invoke('attr', 'title');
  }

  getURLTextbox() {
    return cy.get('#qrcodeUrl');
  }

  createQRCodeButton() {
    return cy.get('#button-create-qr-code');
  }

  downloadQRCodeInPNGButton() {
    return cy.get('#button-download-qr-code-png');
  }

  downloadQRCodeInSVGButton() {
    return cy.get('[ng-click="download(\'svg\')"]');
  }

  downloadQRCodeInPDFButton() {
    return cy.get('[ng-click="download(\'pdf\')"]');
  }

  downloadQRCodeInEPSButton() {
    return cy.get('[ng-click="download(\'eps\')"]');
  }

  downloadQRCodeStatusText() {
    return cy.get('.original-variant > .content > .download-status > .text');
  }

  getQRCodePreview() {
    return cy.get('img.card-img-top');
  }

  getIncorrectURLError() {
    return cy.get('.ng-submitted > .form-group > .error-text');
  }

  getAlertMessage() {
    return cy.get('.alert-danger');
  }
}

export default Home;
