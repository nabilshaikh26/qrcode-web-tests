/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/no-unresolved */
/* eslint-disable linebreak-style */
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import { addMatchImageSnapshotCommand } from 'cypress-image-snapshot/command';

addMatchImageSnapshotCommand();

Cypress.Commands.add('readDownloadedFile', (format) => {
  if (format === 'png') {
    cy
      .readFile(`cypress/downloads/qr-code.${format}`, 'base64', { timeout: 15000 })
      .then((base64) => `data:image/png;base64,${base64}`)
      .then((imageSrc) => imageSrc);
  } else if (format === 'svg') {
    cy
      .readFile(`cypress/downloads/qr-code.${format}`, 'base64', { timeout: 15000 })
      .then((base64) => `data:image/svg+xml;base64,${base64}`)
      .then((imageSrc) => imageSrc);
  } else if (format === 'pdf') {
    cy
      .readFile(`cypress/downloads/qr-code.${format}`, 'base64', { timeout: 15000 })
      .then((base64) => `data:application/pdf;base64,${base64}`)
      .then((imageSrc) => imageSrc);
  } else if (format === 'eps') {
    cy
      .readFile(`cypress/downloads/qr-code.${format}`, 'base64', { timeout: 15000 })
      .then((base64) => `data:application/postscript;base64,${base64}`)
      .then((imageSrc) => imageSrc);
  } else {
    throw new Error('Invalid file format selected!');
  }
});
